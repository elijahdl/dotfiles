filetype plugin on
syntax on
set number
set mouse=a
let g:tex_flavor = 'latex'

call plug#begin('~/.local/share/nvim/plugged')
Plug 'lervag/vimtex'
Plug 'dylanaraps/wal.vim'
Plug 'chrisbra/Colorizer'
Plug 'christoomey/vim-tmux-navigator'
Plug 'robertbasic/vim-hugo-helper'
call plug#end()

" This needs the python3 version of neovim-remote installed
let g:vimtex_compiler_progname = 'nvr'

"set foldmethod=indent
" Keep all folds open when file is opened
augroup OpenAllFoldsOnFileOpen
	autocmd!
	autocmd BufRead * normal zR
augroup END

" C ide behaviour
"set exrc
"set secure

set colorcolumn=100
highlight ColorColumn ctermbg=darkgray

" Set options for filetypes
if !exists("autocommands_loaded")
	let autocommands_loaded = 1
	"autocmd BufNewFile,BufRead *.txt,*.md set spell
endif

" Set yaml spacing
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Persistent undo
set undodir=~/.vim/undodir
set undofile

set spell
set spelllang=en
set spellfile=$HOME/.config/nvim/spell/en.utf-8.add

" Use clipboard for yank and paste
set clipboard+=unnamed

" Use 24-bit colors
set termguicolors

" Remap split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
