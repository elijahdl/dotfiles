#!/bin/bash

result="$(yay -Qqu | wc -l)"
re='^[0-9]+$'

if ! [[ $result =~ $re ]] ; then
  echo "~"
else
  echo $result
fi
