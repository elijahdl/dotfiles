#!/bin/bash
source ~/.cache/wal/colors.sh

swaylock -f -c $color0 \
	--inside-color $color5 \
	--inside-clear-color $color5 \
	--bs-hl-color $color5 \
	--caps-lock-bs-hl-color $color5 \
	--caps-lock-key-hl-color $color5 \
	--inside-ver-color $color12 \
	--inside-wrong-color $color13 \
	--key-hl-color $color7 \
	--layout-text-color $color0 \
	--line-color $color7 \
	--line-caps-lock-color FF0000 
