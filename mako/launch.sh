#!/bin/bash

# Source colors from wal
source ~/.cache/wal/colors.sh

# Terminate already running bar instances
killall -q mako

# Wait until the processes have been shut down
while pgrep -u $UID -x mako >/dev/null; do sleep 1; done

# Launch mako, using default config location ~/.config/mako/config
mako --layer overlay --background-color $color0 --text-color $color7 --border-size 2 --border-color $color2 &

echo "mako launched..."
